CXX_FLAGS=-Wall -Wextra
SOURCE=source
O3: CXX_FLAGS+=-O3
O3: streamcluster streamcluster_omp

O0: CXX_FLAGS+=-O0
O0: streamcluster streamcluster_omp

O3SIMD: CXX_FLAGS+=-O3 -DSIMD
O3SIMD: streamcluster streamcluster_omp

O0SIMD: CXX_FLAGS+=-O0 -DSIMD
O0SIMD: streamcluster streamcluster_omp

streamcluster: $(SOURCE)/streamcluster.cpp
	g++ $(SOURCE)/streamcluster.cpp -o streamcluster $(CXX_FLAGS)

streamcluster_omp: $(SOURCE)/streamcluster_omp.cpp
	g++ $(SOURCE)/streamcluster_omp.cpp -o streamcluster_omp $(CXX_FLAGS) -fopenmp

clean:
	rm -f streamcluster
	rm -f streamcluster_omp
	rm -f *.parallel
	rm -f *.serial
