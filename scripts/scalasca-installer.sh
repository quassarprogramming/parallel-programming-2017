#!/bin/bash

# Instructions
# 1. run chmod +x wizard.sh
# 2. run ./wizard.sh

clear

echo 'Parallel Programming Toolkit Automated Installation Wizard'

echo 'WARNING: DO NOT CLOSE THIS WINDOW !!!'

sleep 10

sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade

sudo apt-get install bison flex gfortran g++ gcc-5-plugin-dev libqt4-dev qt4-qmake zlib1g-dev freeglut3-dev binutils-dev

cd ${HOME}

wget http://icl.utk.edu/projects/papi/downloads/papi-5.5.1.tar.gz
wget http://www.vi-hps.org/upload/packages/opari2/opari2-2.0.1.tar.gz
wget http://apps.fz-juelich.de/scalasca/releases/cube/4.3/dist/cube-4.3.4.tar.gz
wget http://www.vi-hps.org/upload/packages/scorep/scorep-3.0.tar.gz
wget http://apps.fz-juelich.de/scalasca/releases/scalasca/2.3/dist/scalasca-2.3.1.tar.gz
wget https://gist.githubusercontent.com/xorz57/64530b24948e0a3d9362b1732ec1677e/raw/564c913051f6d46245252ff2cf8947ab4ec18f35/disable_nmi_watchdog.sh

cd ${HOME}

tar -xzf papi-5.5.1.tar.gz
tar -xzf opari2-2.0.1.tar.gz
tar -xzf cube-4.3.4.tar.gz
tar -xzf scorep-3.0.tar.gz
tar -xzf scalasca-2.3.1.tar.gz

cd ${HOME}

rm -f papi-5.5.1.tar.gz
rm -f opari2-2.0.1.tar.gz
rm -f cube-4.3.4.tar.gz
rm -f scorep-3.0.tar.gz
rm -f scalasca-2.3.1.tar.gz

clear

echo 'The compilation is going to take some time.'
echo 'No user interraction is needed at this step.'
echo 'You can go make yourself a cup of coffee :)'

sleep 10

clear

chmod +x disable_nmi_watchdog.sh
sudo su -c "./disable_nmi_watchdog.sh"

cd papi-5.5.1/src
./configure --prefix=${HOME}/opt/papi-5.5.1
make
make install

cd ${HOME}

cd opari2-2.0.1
./configure --prefix=${HOME}/opt/opari2-2.0.1
make
make install

cd ${HOME}

cd cube-4.3.4
./configure --prefix=${HOME}/opt/cube-4.3.4
make
make install

cd ${HOME}

cd scorep-3.0
./configure --prefix=${HOME}/opt/scorep-3.0 --with-opari2=${HOME}/opt/opari2-2.0.1 --with-cube=${HOME}/opt/cube-4.3.4 --with-papi-header=${HOME}/opt/papi-5.5.1/include --with-papi-lib=${HOME}/opt/papi-5.5.1/lib --without-mpi --disable-gcc-plugin
make
make install

cd ${HOME}

cd scalasca-2.3.1
./configure --prefix=${HOME}/opt/scalasca-2.3.1 --with-opari2=${HOME}/opt/opari2-2.0.1 --with-cube=${HOME}/opt/cube-4.3.4 --with-papi=${HOME}/opt/papi-5.5.1 --with-qmake=/usr/bin/qmake-qt4 --without-mpi
make
make install

cd ${HOME}

rm -f -rf papi-5.5.1
rm -f -rf opari2-2.0.1
rm -f -rf cube-4.3.4
rm -f -rf scorep-3.0
rm -f -rf scalasca-2.3.1

cd ${HOME}

echo 'export PATH=${HOME}/opt/cube-4.3.4/bin:${PATH}' >> .bash_aliases
echo 'export PATH=${HOME}/opt/scorep-3.0/bin:${PATH}' >> .bash_aliases
echo 'export PATH=${HOME}/opt/scalasca-2.3.1/bin:${PATH}' >> .bash_aliases
echo 'export SCOREP_TOTAL_MEMORY=2G' >> .bash_aliases
echo 'SMALL_D="10 20 32 4096 4096 1000 none output_small.txt"' >> .bash_aliases
echo 'MEDIUM_D="10 20 64 8192 8192 1000 none output_medium.txt"' >> .bash_aliases
echo 'LARGE_D="10 20 128 16384 16384 1000 none output_large.txt"' >> .bash_aliases
echo 'SMALL_R="10 20 256 32768 32768 1000 none output_small.txt"' >> .bash_aliases
echo 'MEDIUM_R="10 20 512 65536 65536 1000 none output_medium.txt"' >> .bash_aliases
echo 'LARGE_R="10 20 1024 131072 131072 1000 none output_large.txt"' >> .bash_aliases
echo '[[ "$PWD" =~ parallel-programming-2017 ]] && sudo ~/disable_nmi_watchdog.sh' >> .bash_aliases

clear

echo 'Installation is now complete !'
echo 'You may now close this window now.'
