#!/bin/bash

# These are the command line arguments that take much more time
SMALL_R="10 20 256 32768 32768 1000 none output_small_r.txt.parallel"
MEDIUM_R="10 20 512 65536 65536 1000 none output_medium_r.txt.parallel"
LARGE_R="10 20 1024 131072 131072 1000 none output_large_r.txt.parallel"

# Find out how many hardware threads the CPU has
MAX_THREADS=$(grep -c ^processor /proc/cpuinfo)

# Delete the benchmarks folder
rm -f -rf benchmarks

# Make a directory tree
mkdir benchmarks
mkdir benchmarks/OpenMP
mkdir benchmarks/OpenMP/O3
mkdir benchmarks/OpenMP/O0
mkdir benchmarks/SIMD
mkdir benchmarks/SIMD/O3
mkdir benchmarks/SIMD/O0

# Recompile code with -O3 optimizations
make clean
make O3

# Run the benchmark using 1 hardware thread
export OMP_NUM_THREADS=1
./streamcluster_omp $SMALL_R | awk -v threads=1 '{print threads" "$4}' >> benchmarks/OpenMP/O3/Small.log
./streamcluster_omp $MEDIUM_R | awk -v threads=1 '{print threads" "$4}' >> benchmarks/OpenMP/O3/Medium.log
./streamcluster_omp $LARGE_R | awk -v threads=1 '{print threads" "$4}' >> benchmarks/OpenMP/O3/Large.log
echo ==================

# Run the benchmark using 2, 4, 6, 8, 10, 12 hardware threads
for ((i=2;i<=MAX_THREADS;i+=2))
do
export OMP_NUM_THREADS=$i
./streamcluster_omp $SMALL_R | awk -v threads=$i '{print threads" "$4}' >> benchmarks/OpenMP/O3/Small.log
./streamcluster_omp $MEDIUM_R | awk -v threads=$i '{print threads" "$4}' >> benchmarks/OpenMP/O3/Medium.log
./streamcluster_omp $LARGE_R | awk -v threads=$i '{print threads" "$4}' >> benchmarks/OpenMP/O3/Large.log
echo ==================
done

# Recompile code with -O0 optimizations
make clean
make O0

# Run the benchmark using 1 hardware thread
export OMP_NUM_THREADS=1
./streamcluster_omp $SMALL_R | awk -v threads=1 '{print threads" "$4}' >> benchmarks/OpenMP/O0/Small.log
./streamcluster_omp $MEDIUM_R | awk -v threads=1 '{print threads" "$4}' >> benchmarks/OpenMP/O0/Medium.log
./streamcluster_omp $LARGE_R | awk -v threads=1 '{print threads" "$4}' >> benchmarks/OpenMP/O0/Large.log
echo ==================

# Run the benchmark using 2, 4, 6, 8, 10, 12 hardware threads
for ((i=2;i<=MAX_THREADS;i+=2))
do
export OMP_NUM_THREADS=$i
./streamcluster_omp $SMALL_R | awk -v threads=$i '{print threads" "$4}' >> benchmarks/OpenMP/O0/Small.log
./streamcluster_omp $MEDIUM_R | awk -v threads=$i '{print threads" "$4}' >> benchmarks/OpenMP/O0/Medium.log
./streamcluster_omp $LARGE_R | awk -v threads=$i '{print threads" "$4}' >> benchmarks/OpenMP/O0/Large.log
echo ==================
done

# Recompile code with -O3 optimizations
make clean
make O3SIMD

# Run the benchmark using 1 hardware thread
export OMP_NUM_THREADS=1
./streamcluster_omp $SMALL_R | awk -v threads=1 '{print threads" "$4}' >> benchmarks/SIMD/O3/Small.log
./streamcluster_omp $MEDIUM_R | awk -v threads=1 '{print threads" "$4}' >> benchmarks/SIMD/O3/Medium.log
./streamcluster_omp $LARGE_R | awk -v threads=1 '{print threads" "$4}' >> benchmarks/SIMD/O3/Large.log
echo ==================

# Run the benchmark using 2, 4, 6, 8, 10, 12 hardware threads
for ((i=2;i<=MAX_THREADS;i+=2))
do
export OMP_NUM_THREADS=$i
./streamcluster_omp $SMALL_R | awk -v threads=$i '{print threads" "$4}' >> benchmarks/SIMD/O3/Small.log
./streamcluster_omp $MEDIUM_R | awk -v threads=$i '{print threads" "$4}' >> benchmarks/SIMD/O3/Medium.log
./streamcluster_omp $LARGE_R | awk -v threads=$i '{print threads" "$4}' >> benchmarks/SIMD/O3/Large.log
echo ==================
done

# Recompile code with -O0 optimizations
make clean
make O0SIMD

# Run the benchmark using 1 hardware thread
export OMP_NUM_THREADS=1
./streamcluster_omp $SMALL_R | awk -v threads=1 '{print threads" "$4}' >> benchmarks/SIMD/O0/Small.log
./streamcluster_omp $MEDIUM_R | awk -v threads=1 '{print threads" "$4}' >> benchmarks/SIMD/O0/Medium.log
./streamcluster_omp $LARGE_R | awk -v threads=1 '{print threads" "$4}' >> benchmarks/SIMD/O0/Large.log
echo ==================

# Run the benchmark using 2, 4, 6, 8, 10, 12 hardware threads
for ((i=2;i<=MAX_THREADS;i+=2))
do
export OMP_NUM_THREADS=$i
./streamcluster_omp $SMALL_R | awk -v threads=$i '{print threads" "$4}' >> benchmarks/SIMD/O0/Small.log
./streamcluster_omp $MEDIUM_R | awk -v threads=$i '{print threads" "$4}' >> benchmarks/SIMD/O0/Medium.log
./streamcluster_omp $LARGE_R | awk -v threads=$i '{print threads" "$4}' >> benchmarks/SIMD/O0/Large.log
echo ==================
done

make clean
