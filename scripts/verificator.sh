#!/bin/bash

SERIAL_OUTPUT_SMALL_D=../output_small_d.txt.serial
SERIAL_OUTPUT_MEDIUM_D=../output_medium_d.txt.serial
SERIAL_OUTPUT_LARGE_D=../output_large_d.txt.serial

SERIAL_OUTPUT_SMALL_R=../output_small_r.txt.serial
SERIAL_OUTPUT_MEDIUM_R=../output_medium_r.txt.serial
SERIAL_OUTPUT_LARGE_R=../output_large_r.txt.serial

PARALLEL_OUTPUT_SMALL_D=../output_small_d.txt.parallel
PARALLEL_OUTPUT_MEDIUM_D=../output_medium_d.txt.parallel
PARALLEL_OUTPUT_LARGE_D=../output_large_d.txt.parallel

PARALLEL_OUTPUT_SMALL_R=../output_small_r.txt.parallel
PARALLEL_OUTPUT_MEDIUM_R=../output_medium_r.txt.parallel
PARALLEL_OUTPUT_LARGE_R=../output_large_r.txt.parallel

SMALL_D_PARAMETERS="10 20 32 4096 4096 1000 none ../output_small_d.txt.parallel"
MEDIUM_D_PARAMETERS="10 20 64 8192 8192 1000 none ../output_medium_d.txt.parallel"
LARGE_D_PARAMETERS="10 20 128 16384 16384 1000 none ../output_large_d.txt.parallel"

SMALL_R_PARAMETERS="10 20 256 32768 32768 1000 none ../output_small_r.txt.parallel"
MEDIUM_R_PARAMETERS="10 20 512 65536 65536 1000 none ../output_medium_r.txt.parallel"
LARGE_R_PARAMETERS="10 20 1024 131072 131072 1000 none ../output_large_r.txt.parallel"

EXECUTABLE="../streamcluster_omp"

make -C ../ clean

tar xzf ../serial-output-files.tar.gz -C ../

make -C ../ O3

$EXECUTABLE $SMALL_D_PARAMETERS
diff $SERIAL_OUTPUT_SMALL_D $PARALLEL_OUTPUT_SMALL_D | wc | awk '{print ($1 > 1) ? "fail" : "success"}'

$EXECUTABLE $MEDIUM_D_PARAMETERS
diff $SERIAL_OUTPUT_MEDIUM_D $PARALLEL_OUTPUT_MEDIUM_D | wc | awk '{print ($1 > 1) ? "fail" : "success"}'

$EXECUTABLE $LARGE_D_PARAMETERS
diff $SERIAL_OUTPUT_LARGE_D $PARALLEL_OUTPUT_LARGE_D | wc | awk '{print ($1 > 1) ? "fail" : "success"}'

$EXECUTABLE $SMALL_R_PARAMETERS
diff $SERIAL_OUTPUT_SMALL_R $PARALLEL_OUTPUT_SMALL_R | wc | awk '{print ($1 > 1) ? "fail" : "success"}'

$EXECUTABLE $MEDIUM_R_PARAMETERS
diff $SERIAL_OUTPUT_MEDIUM_R $PARALLEL_OUTPUT_MEDIUM_R | wc | awk '{print ($1 > 1) ? "fail" : "success"}'

$EXECUTABLE $LARGE_R_PARAMETERS
diff $SERIAL_OUTPUT_LARGE_R $PARALLEL_OUTPUT_LARGE_R | wc | awk '{print ($1 > 1) ? "fail" : "success"}'

make -C ../ clean

tar xzf ../serial-output-files.tar.gz -C ../

make -C ../ O3SIMD

$EXECUTABLE $SMALL_D_PARAMETERS
diff $SERIAL_OUTPUT_SMALL_D $PARALLEL_OUTPUT_SMALL_D | wc | awk '{print ($1 > 1) ? "fail" : "success"}'

$EXECUTABLE $MEDIUM_D_PARAMETERS
diff $SERIAL_OUTPUT_MEDIUM_D $PARALLEL_OUTPUT_MEDIUM_D | wc | awk '{print ($1 > 1) ? "fail" : "success"}'

$EXECUTABLE $LARGE_D_PARAMETERS
diff $SERIAL_OUTPUT_LARGE_D $PARALLEL_OUTPUT_LARGE_D | wc | awk '{print ($1 > 1) ? "fail" : "success"}'

$EXECUTABLE $SMALL_R_PARAMETERS
diff $SERIAL_OUTPUT_SMALL_R $PARALLEL_OUTPUT_SMALL_R | wc | awk '{print ($1 > 1) ? "fail" : "success"}'

$EXECUTABLE $MEDIUM_R_PARAMETERS
diff $SERIAL_OUTPUT_MEDIUM_R $PARALLEL_OUTPUT_MEDIUM_R | wc | awk '{print ($1 > 1) ? "fail" : "success"}'

$EXECUTABLE $LARGE_R_PARAMETERS
diff $SERIAL_OUTPUT_LARGE_R $PARALLEL_OUTPUT_LARGE_R | wc | awk '{print ($1 > 1) ? "fail" : "success"}'

make -C ../ clean

